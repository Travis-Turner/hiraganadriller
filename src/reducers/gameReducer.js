import {
    TARGET_UPDATE,
    CHOICES_UPDATE,
    ADD_STREAK,
    RESET_STREAK,
    SET_HIGH_SCORE,
    GENERATE_CARD,
    LOSE_LIFE,
    RESET_LIFES,
    SET_STANDARD_MODE,
    SET_LISTENING_MODE,
    SET_QUIET_MODE,
    START_GAME,
    END_GAME,
    SOUND_ON,
    SOUND_OFF
  } from '../actions/types';
  import { generateRandomArray } from '../assets/scripts/play';
  
  const INITIAL_STATE = {
    targetChar: 'E',
    choices: [],
    randNums: [],
    streak: 0,
    hiScore: 0,
    card: 'standard',
    lives: 3,
    mode: 'standard',
    inProgress: false,
    sound: true
  }
  
  export default (state = INITIAL_STATE, action) => {
    switch(action.type){
      case TARGET_UPDATE:
        return {
          ...state, 
          targetChar: action.payload
        }
      case CHOICES_UPDATE:
        return {
          ...state,
          choices: action.payload,
          randNums: generateRandomArray()
        }
      case ADD_STREAK:
        return {
          ...state, 
          streak: state.streak + 1
        }
      case RESET_STREAK:
        return {
          ...state,
          streak: 0
        }
      case SET_HIGH_SCORE:
        return {
          ...state,
          hiScore: action.payload
        }
      case GENERATE_CARD:
        return {
          ...state,
          card: action.payload
        }
      case LOSE_LIFE:
        return {
          ...state,
          lives: state.lives - 1
        }
      case RESET_LIFES:
        return {
          ...state,
          lives: 3
        }
      case SET_STANDARD_MODE:
        return {
          ...state,
          mode: 'standard'
        }
      case SET_LISTENING_MODE:
        return {
          ...state,
          mode: 'listening'
        }
      case SET_QUIET_MODE:
        return {
          ...state,
          mode: 'quiet'
        }
      case START_GAME:
        return {
          ...state,
          inProgress: true
        }
      case END_GAME:
        return {
          ...state,
          inProgress: false
        }
      case SOUND_ON:
        return {
          ...state,
          sound: true
        }
      case SOUND_OFF:
        return {
          ...state,
          sound: false
        }
      default:
        return state;
    }
  }