import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import MainMenu from './components/MainMenu';
import PracticeMenu from './components/PracticeMenu';
import PlayMenu from './components/PlayMenu';
import Play from './components/Play/Play';
import Practice from './components/Practice/Practice';
import Reference from './components/Reference';

const RouterComponent = () => {
    return (
        <Router>
            <Scene key="root" hideNavBar duration={0} {...mainConfig}>
                <Scene key="menu" duration={0}>
                    <Scene 
                        key="mainMenu"
                        title="mainMenu"
                        component={MainMenu}
                        duration={0}
                        hideNavBar
                    />
                    <Scene 
                        key="practiceMenu"
                        title="practiceMenu"
                        component={PracticeMenu}
                        duration={0}
                        hideNavBar
                    />
                    <Scene 
                        key="playMenu"
                        title="playMenu"
                        component={PlayMenu}
                        duration={0}
                        hideNavBar
                    />
                </Scene>
                <Scene key="game" hideNavBar {...gameConfig}>
                    <Scene 
                    key="play"
                    title="play"
                    component={Play} 
                    duration={0} 
                    />
                    <Scene 
                    key="practice"
                    title="practice"
                    component={Practice} 
                    duration={0} 
                    />
                </Scene>
                <Scene key="other" hideNavBar {...gameConfig}>
                    <Scene 
                        key="reference"
                        title="reference"
                        component={Reference}
                        duration={0}
                    />
                </Scene>
            </Scene>
        </Router>
    );
};

var mainConfig = {
    cardStyle: {
          backgroundColor: 'white'
    }
}
var gameConfig = {
    cardStyle: {
        backgroundColor: '#F4F4F4'
    }
}

export default RouterComponent;