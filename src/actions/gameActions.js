import {
   TARGET_UPDATE,
   CHOICES_UPDATE,
   ADD_STREAK,
   RESET_STREAK,
   SET_HIGH_SCORE,
   SET_STANDARD_MODE,
   SET_LISTENING_MODE,
   SET_QUIET_MODE,
   GENERATE_CARD,
   START_GAME,
   END_GAME,
   SOUND_ON,
   SOUND_OFF
} from './types';

import { generateChoices } from '../assets/scripts/play';
import { generateRandCard } from '../assets/scripts/play';

export const targetUpdate = (target) => {
    return {
        type: TARGET_UPDATE,
        payload: target
    }
};

export const choicesUpdate = () => {
    return {
        type: CHOICES_UPDATE,
        payload: generateChoices()
    }
}

export const addStreak = () => {
    return {
        type: ADD_STREAK
    }
}

export const resetStreak = () => {
    return {
        type: RESET_STREAK
    }
}

export const setHiScore = (target) => {
    return {
        type: SET_HIGH_SCORE,
        payload: target
    }
}

export const generateCard = () => {
    return {
        type: GENERATE_CARD,
        payload: generateRandCard()
    }
}

export const setStandardMode = () => {
    return {
        type: SET_STANDARD_MODE
    }
}

export const setListeningMode = () => {
    return {
        type: SET_LISTENING_MODE
    }
}

export const setQuietMode = () => {
    return {
        type: SET_QUIET_MODE
    }
}

export const startGame = () => {
    return {
        type: START_GAME
    }
}

export const endGame = () => {
    return {
        type: END_GAME
    }
}

export const soundOn = () => {
    return {
        type: SOUND_ON
    }
}

export const soundOff = () => {
    return {
        type: SOUND_OFF
    }
}