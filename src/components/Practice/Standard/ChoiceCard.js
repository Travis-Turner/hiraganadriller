import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AsyncStorage, View, Modal } from 'react-native';
import { choicesUpdate, generateCard } from '../../../actions/gameActions';
import data from '../../../assets/data';
import s from '../../../styles/ChoiceCard';
import Choice from './Choice';
import VicModal from '../../VicModal';

let Sound = require('react-native-sound');
var success = new Sound('hd_success.mp3', Sound.MAIN_BUNDLE, (error) => {
    if (error) {
      return;
    }
  });

class ChoiceCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            0: true,
            1: true,
            2: true,
            3: true
        }
        this.handleChoice = this.handleChoice.bind(this);
    }
    handleChoice(romanization, i) {
        if (romanization === this.props.targetChar){ 
            this.props.generateCard();
            this.props.choicesUpdate();
            success.play();
            this.setState({
                0: true,
                1: true,
                2: true,
                3: true
            })
            return;
        }
        //Game over condition
        //Beat high score condition

        switch(i){     
            case 0:
                this.setState({ 0: false});
                break;
            case 1:
                this.setState({ 1: false});
                break;
            case 2:
                this.setState({ 2: false});
                break;
            case 3:
                this.setState({ 3: false});
                break;
        }
    }
    closeModal() {
        this.props.choicesUpdate();
        this.setState({modalVisible:false});
      }
    render () {
    //This code depends heavily on a number of random variables.
    //Arrays and objects are being accessed by randomly generated integers.
    //The follow two objects help make neater references based on those random variables.
    const choiceObj = {
        0: this.props.choice1,
        1: this.props.choice2,
        2: this.props.choice3,
        3: this.props.choice4
    }
    const testObj = {
        0: data[choiceObj[this.props.randArr[0]]].romanization,
        1: data[choiceObj[this.props.randArr[1]]].romanization,
        2: data[choiceObj[this.props.randArr[2]]].romanization,
        3: data[choiceObj[this.props.randArr[3]]].romanization
    }   
    return (
        <View style={s.mainContainer}>
            <View style={s.row}>
                <Choice 
                    handleChoice={this.handleChoice}
                    index={0}
                    testObj={testObj}
                    picked={this.state[0]}
                />
                <Choice 
                    handleChoice={this.handleChoice}
                    index={1}
                    testObj={testObj}
                    picked={this.state[1]}
                 />
            </View>
            <View style={s.row}>
                <Choice 
                    handleChoice={this.handleChoice}
                    index={2}
                    testObj={testObj}
                    picked={this.state[2]}
                />
                <Choice 
                    handleChoice={this.handleChoice}
                    index={3}
                    testObj={testObj}
                    picked={this.state[3]}
                />
            </View>           
        </View>
        );     
    }
};

const mapStateToProps = (state) => {
    return {
        randArr: state.game.randNums
    }
}

export default connect(mapStateToProps, { choicesUpdate, generateCard })(ChoiceCard);