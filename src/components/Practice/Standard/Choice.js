import React from 'react';
import { Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import s from '../../../styles/ChoiceCard';

const { width, height } = Dimensions.get('window');
const aspectRatio = height/width;

const Choice = (props) => (
    <TouchableOpacity 
        style={props.picked ? s.textContainer : styles.incorrect} 
        onPress={() => {props.handleChoice(props.testObj[props.index], props.index);}}
    >
        <Text style={props.picked ? s.text : styles.incorrectText}>{props.picked ? props.testObj[props.index] : 'X'}</Text>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    incorrect: {
            borderColor: 'rgba(0, 0, 0, 0.1)',
            borderWidth: 1,
            borderRadius: 10,
            backgroundColor: '#F93C3C',
            margin: 10,
            alignItems: 'center',
            justifyContent: 'center',
            width: '40%',
            height: aspectRatio < 1.6 ? 70 : 'auto'
    },
    incorrectText: {
        fontSize: aspectRatio < 1.6 ? 20 : 34,
        padding: 30,
        borderRadius: 10,
        color: '#F93C3C'
    }
});


export default Choice;