import React from 'react';
import { Text, TouchableOpacity, StyleSheet, Dimensions, Image } from 'react-native';
const { width, height } = Dimensions.get('window');
const aspectRatio = height/width;
import data from '../../../assets/data';
import s from '../../../styles/ChoiceCard';

const Choice = (props) => (
        <TouchableOpacity 
            style={styles.imageContainer} 
            onPress={() => {
                props.handleChoice(props.choiceRefs[props.index].romanization, props.index);
            }}
        >
            <Image 
                source={props.picked ? props.choiceRefs[props.index].image : data['Wrong'].image} 
                style={styles.image}
                resizeMode="contain"
            />  
        </TouchableOpacity>
);

const styles = StyleSheet.create({
    image: {
        height: 120,
        width: 150,
        flex: 1
    },
    imageContainer: {
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 100
    }
});


export default Choice;