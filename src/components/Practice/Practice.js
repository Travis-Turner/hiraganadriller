import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Streak from '../Streak';
import Standard from './Standard/Standard';
import Reverse from './Reverse/Reverse';
import TargetCard from './Standard/PracticeTargetCard';
import ChoiceCard from './Standard/ChoiceCard';
import { FadeInView } from '../common';
import { targetUpdate, choicesUpdate, resetStreak } from '../../actions/gameActions'; 

class Practice extends Component {
    constructor(props){
        super(props);
        this.onQuit = this.onQuit.bind(this);
    }
    componentWillMount() {
        this.props.choicesUpdate();
    }
    renderCard(card) {
        switch(card){
            case 'standard':
                return (
                    <Standard 
                        choices={this.props.choices}
                        onQuit={this.onQuit}
                    />
                );
            case 'reverse':
                return (
                    <Reverse
                        choices={this.props.choices}
                        onQuit={this.onQuit}
                    />
                );
            case 'sound':
                return (
                    <Standard 
                        choices={this.props.choices}
                        onQuit={this.onQuit}
                    />
                )
        }
    }
    onQuit () {
        Actions.mainMenu();
    }
    render() {
        return (
            <FadeInView>
                {
                   this.renderCard(this.props.card)
                }
            </FadeInView>
        )
    }
};

const mapStateToProps = (state) => {
    return {
        targetChar: state.game.targetChar,
        choices: state.game.choices,
        card: state.game.card,
        mode: state.game.mode
    }
};



export default connect(mapStateToProps, { targetUpdate, choicesUpdate, resetStreak })(Practice);