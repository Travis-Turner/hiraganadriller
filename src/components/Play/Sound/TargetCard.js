import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import data from '../../../assets/data';
const { width, height } = Dimensions.get('window');
const aspectRatio = height/width;
let Sound = require('react-native-sound');

class TargetCard extends Component { 
    constructor(props){
        super(props);
        this.playSound = this.playSound.bind(this);
        this.handleTouch = this.handleTouch.bind(this);
        this.state = {
            count: 0
        }
    }
    playSound() {
        let sound = new Sound(`${data[this.props.targetChar].romanization}.mp3`, Sound.MAIN_BUNDLE, (error) => {
            if (error) {
              return;
            }
            if (this.props.mode !== 'quiet' && this.props.inProgress && this.props.sound){
                console.log('here', this.props.inProgress);
                console.log('playing!');
                sound.play();
            }    
          });
          sound.release();
    }
    componentWillMount(){
        if (this.props.mode === 'standard'){
            setTimeout(() => {
                this.playSound();
            }, 800);
        }
    }
    componentDidUpdate() {
        setTimeout(() => {
            this.playSound();
        }, 800);
    }
    handleTouch() {
       this.playSound();
    }
    render() {
        return (
            <View style={styles.targetContainer} >
                <TouchableOpacity onPress={this.handleTouch}>
                    <Image 
                        source={data.Sound.image} 
                        style={styles.image}
                        resizeMode="contain"
                    />
                </TouchableOpacity>
            </View>
        )
    }
}
//Mobile aspect ratio is on the right
const styles = StyleSheet.create({
    image: {
        height: aspectRatio < 1.6 ? 150 : 200,
        width: aspectRatio < 1.6 ? 200 : 250
    },
    targetContainer: {
        alignItems: 'center',
        marginTop: 20
    }
});

const mapStateToProps = (state) => {
    return {
        mode: state.game.mode,
        inProgress: state.game.inProgress,
        sound: state.game.sound,
        mode: state.game.mode
    }
}

export default connect(mapStateToProps)(TargetCard);