import React from 'react';
import { View, StyleSheet, Text, Dimensions } from 'react-native';
import data from '../../../assets/data';
const { width, height } = Dimensions.get('window');
const aspectRatio = height/width;

const TargetCard = (props) => {
    return (
        <View style={styles.targetContainer}>
            <View style={styles.textContainer}>
                <View>
                    <Text style={styles.text}>
                        {data[props.targetChar].romanization} 
                    </Text>
                </View>
            </View>
        </View>
    )
}
//Mobile aspect ratio is on the right
const styles = StyleSheet.create({
    targetContainer: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textContainer: {
        borderRadius: 10,
        backgroundColor: 'white',
        height: aspectRatio < 1.6 ? 150 : 200,
        width: aspectRatio < 1.6 ? 200 : 250,
        justifyContent: 'center'
    },
    text: {
        fontSize: aspectRatio < 1.6 ? 24 : 60,
        color: 'black',
        alignSelf: 'center'
    }
});

export default TargetCard;