import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { resetStreak } from '../actions/gameActions';
import { Button } from './common';
import data from '../assets/data';

class VicModal extends Component {
    handlePlayAgain() {
        this.props.closeModal();
    }
    handleHome() {
        Actions.mainMenu();
        this.props.closeModal();
    }
    render(){
        return (
            <View style={{marginTop: 70, alignItems: 'center'}}>
                <Text style={{fontSize: 40}}>GAME OVER</Text>
                <Text style={{fontSize: 24}}>
                    Your final score was {this.props.streak}
                </Text>
                {
                    this.props.beatScore &&
                    <Text style={styles.hiScore}>You beat your high score!</Text>
                }
                <View style={styles.imageContainer}>
                    <Image 
                        source={data[this.props.targetChar].image} 
                        style={styles.image}
                        resizeMode="contain"
                    />
                </View>
                <Text style={styles.correct}>The answer was {data[this.props.targetChar].romanization}</Text>
                <View style={{flexDirection: 'row', marginTop: 40}}>
                    <Button onPress={this.handlePlayAgain.bind(this)}>PLAY AGAIN</Button>
                    <Button onPress={this.handleHome.bind(this)}>HOME</Button>
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    correct: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    modalContainer: {
        marginTop: 70,
        alignItems: 'center'
    },
    gameOver: {
        fontSize: 40
    },
    scoreRead: {
        fontSize: 24
    },
    buttonContainer: {
        flexDirection: 'row',
        marginTop: 40
    },
    image: {
        height: 120,
        width: 150,
        flex: 1
    },
    imageContainer: {
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 100
    },
    hiScore: {
        color: 'red'
    }
});

const mapStateToProps = (state) => {
    return {
        card: state.game.card
    }
}

export default connect(mapStateToProps, { resetStreak })(VicModal);