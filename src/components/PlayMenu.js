import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AsyncStorage, View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Scoreboard from './Scoreboard';
import { Button, Card, CardSection, Header } from './common';
import { 
    targetUpdate, choicesUpdate, setHiScore, 
    setStandardMode, setListeningMode, setQuietMode, startGame,
    soundOff, soundOn, endGame
} from '../actions/gameActions'; 

class PlayMenu extends Component {
    componentWillMount() {
        this.props.setQuietMode();
        this.props.endGame();
        this.props.targetUpdate();
        this.props.choicesUpdate();
        try {
            let hiScore = AsyncStorage.getItem('hi-score');
            hiScore.then((value) => {
                this.props.setHiScore(value);
            })
          } catch (error) {
            
          }
    }
    onPlayPress() {
        this.props.setStandardMode();
        this.props.soundOn();
        this.props.startGame();
        Actions.play();
    }
    onListenPress() {
        this.props.setListeningMode();
        this.props.soundOn();
        this.props.startGame();
        Actions.play();
    }
    onReadPress() {
        this.props.setStandardMode();
        this.props.soundOff();
        this.props.startGame();
        Actions.play();
    }
    onBack() {
        Actions.mainMenu();
    }
    render() {
        return (
            <View>
                <View style={styles.imageContainer}>
                    <Image 
                        source={require('../assets/images/LOGO.png')}
                        style={styles.image}
                        resizeMode="contain"
                    />
                </View>
                <Card>
                    <CardSection>
                        <Scoreboard hiScore={this.props.hiScore} />
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onPlayPress.bind(this)}>
                            ARCADE MODE
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onListenPress.bind(this)}>
                            LISTENING MODE
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onReadPress.bind(this)}>
                            READING MODE
                        </Button>
                    </CardSection>
                    <CardSection>
                        <TouchableOpacity onPress={this.onBack.bind(this)} style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>BACK</Text>
                        </TouchableOpacity>
                </CardSection>
                </Card>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250
    },
    imageContainer: {
        alignItems: 'center',
        marginTop: 40
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#1A99D1',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'white',
        marginLeft: 5,
        marginRight: 5
      },
      textStyle: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 24,
        fontWeight: '600',
        paddingTop: 20,
        paddingBottom: 20
      }
})

const mapStateToProps = (state) => {
    return {
        targetChar: state.game.targetChar,
        choices: state.game.choices,
        hiScore: state.game.hiScore
    }
}

export default connect(mapStateToProps, { 
    targetUpdate, choicesUpdate, setHiScore, setListeningMode, setStandardMode, setQuietMode,
    startGame, soundOn, soundOff, endGame
})(PlayMenu);