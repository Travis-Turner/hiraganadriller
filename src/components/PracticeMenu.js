import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AsyncStorage, View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Card, CardSection, Header } from './common';
import { 
    targetUpdate, choicesUpdate, setHiScore, setStandardMode, 
    setListeningMode, setQuietMode, startGame, endGame
} from '../actions/gameActions'; 

class PracticeMenu extends Component {
    componentWillMount() {
        this.props.setQuietMode();
        this.props.endGame();
        this.props.targetUpdate();
        this.props.choicesUpdate();
        try {
            let hiScore = AsyncStorage.getItem('hi-score');
            hiScore.then((value) => {
                this.props.setHiScore(value);
            })
          } catch (error) {
            
          }
        
    }
    onPracticePress() {
        this.props.setStandardMode();
        this.props.setQuietMode();
        Actions.practice();
    }
    onReferencePress() {
        Actions.reference();
    }
    onBack() {
        Actions.mainMenu();
    }
    render() {
        return (
            <View>
                <View style={styles.imageContainer}>
                    <Image 
                        source={require('../assets/images/LOGO.png')}
                        style={styles.image}
                        resizeMode="contain"
                    />
                </View>
                <Card>
                    <CardSection>
                        <Text>Practice makes perfect!</Text>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onPracticePress.bind(this)}>
                            PRACTICE MODE
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onReferencePress.bind(this)}>
                            REFERENCE
                        </Button>
                    </CardSection>
                    <CardSection>
                        <TouchableOpacity onPress={this.onBack.bind(this)} style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>BACK</Text>
                        </TouchableOpacity>
                    </CardSection>
                </Card>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250
    },
    imageContainer: {
        alignItems: 'center',
        marginTop: 40
    },
    card: {
        marginTop: 25,
        padding: 20,
        backgroundColor: 'rgba(255,255,255,0.7)'
     },
     text: {
         color: 'black'
     },
     buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#1A99D1',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'white',
        marginLeft: 5,
        marginRight: 5
      },
      textStyle: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 24,
        fontWeight: '600',
        paddingTop: 20,
        paddingBottom: 20
      }
})

const mapStateToProps = (state) => {
    return {
        targetChar: state.game.targetChar,
        choices: state.game.choices,
        hiScore: state.game.hiScore,
        inProgress: state.game.inProgress,
        mode: state.game.mode
    }
}

export default connect(mapStateToProps, { 
    targetUpdate, choicesUpdate, setHiScore, setListeningMode, setStandardMode, setQuietMode,
    startGame, endGame
})(PracticeMenu);