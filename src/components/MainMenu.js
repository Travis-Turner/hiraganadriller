import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Card, CardSection } from './common';
import { endGame } from '../actions/gameActions'; 

class MainMenu extends Component {
    componentWillMount(){
        this.props.endGame();
    }
    onPlayPress() {
        Actions.playMenu();
    }
    onPracticePress() {
        Actions.practiceMenu();
    }
    render() {
        return (
            <View>
                <View style={styles.imageContainer}>
                    <Image 
                        source={require('../assets/images/LOGO.png')}
                        style={styles.image}
                        resizeMode="contain"
                    />
                </View>
                <Card>
                    <CardSection>
                        <Text>Time to master the Hiragana!</Text>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onPlayPress.bind(this)}>
                            PLAY
                        </Button>
                    </CardSection>
                    <CardSection>
                        <Button onPress={this.onPracticePress.bind(this)}>
                            PRACTICE
                        </Button>
                    </CardSection>
                </Card>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: 250
    },
    imageContainer: {
        alignItems: 'center',
        marginTop: 40
    }
})


export default connect(null, { endGame })(MainMenu);